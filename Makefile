CC = gcc
CFLAGS = -O2 -Wall

tlc: tlc.c
	$(CC) $(CFLAGS) -s -DUSE_LIBUSB -o tlc tlc.c -lusb

clean:
	rm -f tlc
